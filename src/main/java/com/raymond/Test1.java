package com.raymond;

/**
 * 测试类
 *
 * @author :  raymond
 * @version :  V1.0
 */
public class Test1 {
    private int id;

    public Test1() {
    }

    public Test1(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
